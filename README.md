<h1 align="center">Convert Route Laravel To VueJS</h1>

## About

Laravel 5 package that allows you to share your [Laravel Routes](https://laravel.com/docs/7.x/routing)
with your [vue](http://vuejs.org/) front-end,
using [laravel-route-to-vuejs](https://gitlab.com/TanDD/laravel-route-to-vuejs).

## Laravel 5.7 notice!

Configuration paths have changed in Laravel 5.7, in order for this package to function properly you need to configure
correct paths for jsFile in your `config\laravel-route-to-vue-generator.php`.

## Install the package

In your project:

```
composer require tandd/laravel-route-to-vuejs --dev
```

### For Laravel 5.4 and below:

For older versions of the framework:

Register the service provider in ``config/app.php``

```php
TanDD\LaravelRouteToVueJS\VueJSRouteGeneratorProvider::class,
```

Next, publish the package default config:

```
php artisan vendor:publish --provider="TanDD\LaravelRouteToVueJS\VueJSRouteGeneratorProvider"
```

## Ignore route

From Version 4.0.0 you can set the route want to ignore

in the file `laravel-route-to-vue-generator.php` you can add the route or route's prefix to array `ignoreRoute`

for example:

```php
.
.
'ignoreRoute' => [
                    'name'   => [
                        'ignition',
                    ],
                    'prefix' => [
                        null,
                        '_debugbar',
                        'sanctum',
                        '_ignition',
                    ],
                ],
.
.
```

## Ignore fields output

From Version 4.0.0 you can set the fields if you want to ignore in the output file. The list you can skip export to the
output are:
`['domain','prefix','method','action']`

you can add them to the `ignoreFields` for ignore the field

Config example:
```phpregexp
'ignoreFields'       => [
                            'domain',
                            'prefix',
                            'method',
                            'action',
                        ],
```

## Using laravel-route-to-vuejs in VueJS3

Generate the include file with

```
php artisan laravel-route-to-vue:generate
```

Then defined in Vue main.js file:

```VueJs
import {createApp} from 'vue'
import App from './App.vue'
import LaravelRoute from '@/plugins/laravel-route-to-vue'; //Path to the index file

const app = createApp(App);
app.use(LaravelRoute).mount('#app')

```
### Using in the plugin

```vue
import {routeApi} from "@/plugins/laravel-route-to-vue/laravel-routes.generated"; //Path to the generated directiory

console.log(routeApi('route.name'))
```

### Using in template

Vue template:

```vue

<div class="age">
    <p>{{ proxy.routeApi('route.name') }}</p>
</div>

<script setup>
    import {getCurrentInstance} from "vue";
    const {proxy} = getCurrentInstance();
</script>
```

Vue template with the params:

```vue

<div class="age">
    <p>{{ proxy.routeApi('route.name',{param1:value, param2:value}) }}</p>
</div>

<script setup>
    import {getCurrentInstance} from "vue";
    const {proxy} = getCurrentInstance();
</script>
```

Vue script:

```vue

<script setup>
    import {getCurrentInstance} from "vue";
    const {proxy} = getCurrentInstance();
    
    console.log(proxy.routeApi('route.name', {param1: value, param2: value}))
</script>

```

## Using laravel-route-to-vuejs in VueJS

Generate the include file with

```
php artisan laravel-route-to-vue:generate
```

Then defined in Vue main.js file:

```VueJs
import Vue from 'vue';

import LaravelRoutes from './laravel-route-to-vue.generated';

Vue.use(LaravelRoutes)
```

### Using in template

Blade template:

```html

<div class="age">
    <p>{{ route('route.name') }}</p>
</div>
```

With the param:

```html

<div class="age">
    <p>{{ route('route.name',['name'=>value]) }}</p>
</div>
```

Vue template:

```html

<div class="age">
    <p>{{ route('route.name') }}</p>
</div>
```

Vue template with the params:

```html

<div class="age">
    <p>{{ route('route.name',{param1:value, param2:value}) }}</p>
</div>
```

Vue script:

```javascript
console.log(this.route('route.name', {param1: value, param2: value}))
```

# License

Under [MIT](LICENSE)

# Auth

[Tandd](mailto:tandd.dev@gmail.com)
