<?php namespace TanDD\LaravelRouteToVueJS\Commands;

use Illuminate\Console\Command;
use TanDD\LaravelRouteToVueJS\Generator;

class GenerateRoute extends Command{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'laravel-route-to-vue:generate';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = "Generates the Laravel route to VueJS";

	/**
	 * Execute the console command.
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function handle(): void {
		$config    = /** @scrutinizer ignore-call */
			config( 'laravel-route-to-vue-generator' );
		$generator = new Generator( $config );
		$jsDir     = base_path() . $config['jsDir'];
		if ( ! file_exists( $jsDir ) && ! is_dir( $jsDir ) ) {
			mkdir( $jsDir, 0777, true );
		}
		file_put_contents( $jsDir . 'laravel-routes.generated.js', $generator->generateRoutes() );
		file_put_contents( $jsDir . 'index.js', $generator->indexFileContent() );
		if ( $config['showOutputMessages'] ) {
			$this->info( 'Generated list routes at:' . $jsDir );
		}
	}
}
