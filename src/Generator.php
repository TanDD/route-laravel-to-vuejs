<?php

namespace TanDD\LaravelRouteToVueJS;

use Illuminate\Support\Facades\Route;
use TanDD\LaravelRouteToVueJS\Services\JsMin;

class Generator{
	protected array $_listIgnoreRoutes;
	protected mixed $_listIgnoreFields;

	/**
	 * The constructor
	 *
	 * @param array $config
	 */
	function __construct( array $config = [] ) {
		$this->_listIgnoreRoutes = [
			'name'   => [],
			'prefix' => [],
		];
		$this->_listIgnoreFields = [];
		if ( isset( $config['ignoreRoute'] ) && isset( $config['ignoreRoute']['name'] ) ) {
			$this->_listIgnoreRoutes['name'] = $config['ignoreRoute']['name'];
		}
		if ( isset( $config['ignoreRoute'] ) && isset( $config['ignoreRoute']['prefix'] ) ) {
			$this->_listIgnoreRoutes['prefix'] = $config['ignoreRoute']['prefix'];
		}
		if ( isset( $config['ignoreFields'] ) ) {
			$this->_listIgnoreFields = $config['ignoreFields'];
		}
	}

	/**
	 * @return string
	 */
	function generateRoutes(): string {
		$routes = collect( Route::getRoutes() )->map( function ( $route ) {
			$routeName   = $route->getName();
			$routeAction = $route->getAction();
			if ( is_null( $routeName ) || in_array( $routeName, $this->_listIgnoreRoutes['name'] ) || in_array( $routeAction['prefix'], $this->_listIgnoreRoutes['prefix'] ) ) {
				return [];
			}
			$routeData = [
				'domain' => $route->domain(),
				'prefix' => $routeAction['prefix'],
				'method' => implode( '|', $route->methods() ),
				'uri'    => $route->uri(),
				'name'   => $routeName,
				'action' => ltrim( $route->getActionName(), '\\' ),
			];
			foreach ( [ 'domain', 'prefix', 'method', 'action' ] as $ignoreField ) {
				if ( in_array( $ignoreField, $this->_listIgnoreFields ) ) {
					unset( $routeData[ $ignoreField ] );
				}
			}

			return $routeData;
		} )->filter()->all();
		$routesData    = array_combine( array_column( $routes, 'name' ), $routes );
		$jsBody        = json_encode( $routesData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
		$url           = url( '/' );
		$parsedUrl     = parse_url( $url );
		$baseUrl       = $url . '/';
		$baseProtocol  = array_key_exists( 'scheme', $parsedUrl ) ? $parsedUrl['scheme'] : 'http';
		$baseDomain    = array_key_exists( 'host', $parsedUrl ) ? $parsedUrl['host'] : '';
		$basePort      = array_key_exists( 'port', $parsedUrl ) ? $parsedUrl['port'] : 'false';
		$jsFileContent = "const routes = {
        routeNames :        {$jsBody},
        baseUrl: '$baseUrl',
        baseProtocol: '$baseProtocol',
        baseDomain: '$baseDomain',
        basePort: $basePort
    };
const routeApi = (routeName, params = []) => {
    let routeUrl = routes.routeNames[routeName]['uri'];
    for (const [param, value] of Object.entries(params)) {
        routeUrl = routeUrl.replace('{' + param + '?}', value);
        routeUrl = routeUrl.replace('{' + param + '}', value);
    };

    return routes.baseUrl + routeUrl;
};
export {routeApi};";
		if ( app()->environment( 'production' ) ) {
			return JsMin::minify($jsFileContent);
		}

		return $jsFileContent;
	}

	function indexFileContent(): string {
		$jsFileContent = 'import {routeApi} from "./laravel-routes.generated";
		
export default {
    install(Vue) {
        Vue.config.globalProperties.routeApi = routeApi
    }
};';
		if ( app()->environment( 'production' ) ) {
			return JsMin::minify($jsFileContent);
		}

		return $jsFileContent;
	}
}
