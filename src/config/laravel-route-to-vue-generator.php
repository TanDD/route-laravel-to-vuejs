<?php

return [

	/*
	 *-------------------------------------------------------------------------
	 * Ignore route name, or Prefix
	 *-------------------------------------------------------------------------
	 *
	 */
	'ignoreRoute'        => [
		'name'   => [
			'ignition',
		],
		'prefix' => [
			null,
			'_debugbar',
		],
	],

	/*
	 *-------------------------------------------------------------------------
	 * Ignore fields
	 *-------------------------------------------------------------------------
	 * list fields you can skip ['domain','prefix','method','action']
	 *
	 */
	'ignoreFields'       => [
		'domain',
		'prefix',
		'method',
		'action',
	],
	/*
	|--------------------------------------------------------------------------
	| Output file
	|--------------------------------------------------------------------------
	|
	| The javascript path where I will place the generated file.
	| Note: the path will be prepended to point to the App directory.
	|
	*/
	'jsDir'              => '/resources/js/laravel-route-to-vue/',

	/*
	|--------------------------------------------------------------------------
	| Output messages
	|--------------------------------------------------------------------------
	|
	| Specify if the library should show "written to" messages
	| after generating json files.
	|
	*/
	'showOutputMessages' => true,

];
