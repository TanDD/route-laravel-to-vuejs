<?php

namespace TanDD\LaravelRouteToVueJS;

use Illuminate\Support\ServiceProvider;

class VueJSRouteGeneratorProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('laravel-route-to-vue.generate', function () {
            return new Commands\GenerateRoute;
        });

        $this->commands('laravel-route-to-vue.generate');
        $this->publishes([
            __DIR__.'/config/laravel-route-to-vue-generator.php' => /** @scrutinizer ignore-call */ config_path('laravel-route-to-vue-generator.php'),
        ]);
        $this->mergeConfigFrom(__DIR__.'/config/laravel-route-to-vue-generator.php', 'laravel-route-to-vue-generator');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['laravel-route-to-vue-generator'];
    }
}
